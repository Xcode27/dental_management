<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\queue;
use App\Events\CounterEvent;
use DB;

class QueueController extends Controller
{
    //

    public function counter(){
        return view('counter_window');
    }

    public function counterController(){
        return view('counter_controller');
    }

    public function currentNumber(){

        $currentDate = date('Y-m-d');
        $counter = DB::table('queues')->select('queued_number')->where(DB::raw('DATE(created_at)'),$currentDate)
                //    ->where('status','0')
                    ->orderBy('id','DESC')->first();

        $number = 0;
        
        if(!empty($counter)){
            $number = $counter->queued_number + 1;
        }

        return $number;
    }

    public function nextTransaction(Request $request){
        $queue = new queue;
        $queue->counter = 1;
        $queue->queued_number = $request->cur_num;
        $queue->status = '1';
        $queue->save();

        event(new CounterEvent($request->cur_num));
        return 'success';
    }
}
