$('#login_submit').click(function(e) {
    e.preventDefault();

    var btn = $(this);
    var form = $(this).closest('form');

    form.validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            }
        }
    });

    if (!form.valid()) {
        return;
    }


    btn.attr('disabled', true);
    var data = new FormData(form[0]);

    //$('#msg').css('display', 'block');

    $.ajax({
        type: 'POST',
        url: '/authenticate',
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('#msg').empty();
        },
        success: function(result) {
            //toastr.clear();

            if (result == 'success') {
                location.href = '/dashboard';
            } else if (result == 'no_user') {
                $('#msg').html(alert_error('Username and password does\'nt match.')).delay(4000).fadeOut('slow');
                // btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else if (result == 'not_verified') {
                $('#msg').html(alert_error('Please verify your account. Check your email')).delay(4000).fadeOut('slow');
                // btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            } else {    
                $('#msg').html(alert_error('Ooops! something went wrong'));
                //btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            }
            btn.attr('disabled', false);
        }
    });

});

$('#register_btn').click(function(e) {
    e.preventDefault();

    var btn = $(this);
    var form = $(this).closest('form');

    // form.validate({
    //     rules: {
    //         fname: {
    //             required: true,
               
    //         },
    //         mname: {
    //             required: true,
               
    //         },
    //         lname: {
    //             required: true,
               
    //         },
    //         province: {
    //             required: true,
              
    //         },
    //         city: {
    //             required: true,
              
    //         },
    //         brgy: {
    //             required: true,
              
    //         },
    //         contact: {
    //             required: true,
               
    //         },
    //         birthdate: {
    //             required: true,
               
    //         },
    //         email: {
    //             required: true,
    //             email: true
    //         },
    //         password: {
    //             required: true
    //         }
    //     }
    // });

    if($('#fname').val() == ''){
        $('#err-fname').html('This field is required')
        return false
    }else{
        $('#err-fname').html('')
    }

    if($('#lname').val() == ''){
        $('#err-lname').html('This field is required')
        return false
    }else{
        $('#err-lname').html('')
    }

    if($('#contact').val() == ''){
        $('#err-contact').html('This field is required')
        return false
    }else{
        $('#err-contact').html('')
    }

    if($('#birthdate').val() == ''){
        $('#err-birthdate').html('This field is required')
        return false
    }else{
        $('#err-birthdate').html('')
    }

    if($('#prov').val() == ''){
        $('#err-province').html('This field is required')
        return false
    }else{
        $('#err-province').html('')
    }

    if($('#ct').val() == ''){
        $('#err-municipality').html('This field is required')
        return false
    }else{
        $('#err-municipality').html('')
    }

    if($('#bry').val() == ''){
        $('#err-brgy').html('This field is required')
        return false
    }else{
        $('#err-brgy').html('')
    }

    // if($('#brgy').val() == ''){
    //     $('#err-brgy').html('This field is required')
    //     return false
    // }

    if($('#email').val() == ''){
        $('#err-email').html('This field is required')
        return false
    }else{
        $('#err-email').html('')
    }

    if(!looksLikeMail($('#email').val())){
        $('#err-email').html('Invalid email')
        return false
    }else{
        $('#err-email').html('')
    }

    if($('#pass').val() == ''){
        $('#err-password').html('This field is required')
        return false
    }else{
        $('#err-password').html('')
    }

    if($('#confirm').val() == ''){
        $('#err-confirm').html('This field is required')
        return false
    }else{
        $('#err-confirm').html('')
    }

    if($('#pass').val() != $('#confirm').val()){
        $('#err-confirm').html('Password mismatch')
        return false
    }else{
        $('#err-confirm').html('')
    }

    
    if (!form.valid()) {
        return;
    }


    btn.attr('disabled', true);
    var data = new FormData(form[0]);

    //$('#msg').css('display', 'block');

    $.ajax({
        type: 'POST',
        url: '/registerUser',
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('#msg').empty();
        },
        success: function(result) {
            //toastr.clear();
            if(result != 'success'){
                $('#msg').html(alert_error(result)).delay(4000).fadeOut('slow');
            }else{
                $('#msg').html(alert_success('Successful registered.! An email sent to you')).delay(4000).fadeOut('slow');
            }
            // if (result == 'success') {
            //     location.href = '/dashboard';
            // } else if (result == 'no_user') {
            //     $('#msg').html(alert_error('Username and password does\'nt match.')).delay(4000).fadeOut('slow');
            //     // btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            // } else {
            //     $('#msg').html(alert_error('Ooops! something went wrong'));
            //     //btn.removeClass('kt-spinner kt-spinner--right kt-spinner--sm kt-spinner--light').attr('disabled', false);
            // }
            btn.attr('disabled', false);
        }
    });

});

$('#btn_fp').click(function(e){
    e.preventDefault();

    var btn = $(this);
    var form = $(this).closest('form');

    form.validate({
        rules: {
            email: {
                required: true,
                email: true
            },
        }
    });

    if (!form.valid()) {
        return;
    }


    btn.attr('disabled', true);
    var data = new FormData(form[0]);

    //$('#msg').css('display', 'block');

    $.ajax({
        type: 'POST',
        url: '/resetPassword',
        data: data,
        processData: false,
        contentType: false,
        beforeSend: function(){
            $('#msg').empty();
        },
        success: function(result) {
            //toastr.clear();
            if(result != 'success'){
                $('#msg').html(alert_error(result)).delay(4000).fadeOut('slow');
            }else{
                $('#msg').html(alert_success('Please check your email for default password')).delay(4000).fadeOut('slow');
            }
            
            btn.attr('disabled', false);
        }
    });
})

getAllProvinces()

$('#province').change(function(){
    getAllMunicipality(this.value)

    $('#prov').val($( "#province option:selected" ).text())
})


function getAllProvinces(){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/provinces',
        success: function(result) {
             let obj = JSON.parse(result)
             let html = `<option value="" disabled>Please select province</option>`
                
                obj.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#province').html(html)
        }
    });
}

function getAllMunicipality(provinceCode){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/municipalities',
        success: function(result) {
             let obj = JSON.parse(result)
             obj.push({
                code: "112233",
                districtCode: false,
                isCapital: false,
                islandGroupCode: "luzon",
                name: "Calapan",
                oldName:"",
                provinceCode:"175200000",
                psgc10DigitCode:"1705211000",
                regionCode:"170000000",
             })
             
             let html = `<option value="" disabled>Please select municipality</option>`
             let objResult = obj.filter(data => { return data.provinceCode == provinceCode})
             objResult.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#city').html(html)
        }
    });
}

$('#city').change(function(){
    getAllBarangay(this.value)
    $('#ct').val($( "#city option:selected" ).text())
})

function getAllBarangay(cityCode){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/barangays',
        success: function(result) {
             let obj = JSON.parse(result)





// Malamig
// Managpi
// Masipit
// Nag-Iba I
// Navotas
// Pachoca
// Palhi
// Panggalaan
// Parang
// Patas
// Personas
// Puting Tubig
// San Raphael (formerly Salong)
// San Antonio
// San Vicente Central
// San Vicente East
// San Vicente North
// San Vicente South
// San Vicente West
// Sta. Cruz
// Sta. Isabel
// Sto. Niño (formerly Nacoco)
// Sapul
// Silonay
// Sta. Maria Village
// Suqui
// Tawagan
// Tawiran
// Tibag
// Wawa
// Nag-Iba II
             obj.push(
                {
                    municipalityCode:"112233",
                name:"Balingayan"
                },
                {
                    municipalityCode:"112233",
                    name:"Balite"
                },
                {
                    municipalityCode:"112233",
                    municipalityCode:"Baruyan"
                },
                {
                    municipalityCode:"112233",
                    name:"Batino"
                },
                {
                    municipalityCode:"112233",
                    name:"Bayanan I"
                },
                {
                    municipalityCode:"112233",
                    name:"Bayanan II"
                },
                {
                    municipalityCode:"112233",
                    name:"Biga"
                },
                {
                    municipalityCode:"112233",
                    name:"Bondoc"
                },
                {
                    municipalityCode:"112233",
                    name:"Bucayao"
                },
                {
                    municipalityCode:"112233",
                    name:"Buhuan"
                },
                {
                    municipalityCode:"112233",
                    name:"Bulusan"
                },
                {
                    municipalityCode:"112233",
                    name:"Sta. Rita"
                },
                {
                    municipalityCode:"112233",
                    name:"Calero"
                },
                {
                    municipalityCode:"112233",
                    name:"Camansihan"
                },
                {
                    municipalityCode:"112233",
                    name:"Camilmil"
                },
                {
                    municipalityCode:"112233",
                    name:"Canubing I"
                },
                {
                    municipalityCode:"112233",
                    name:"Canubing II"
                },
                {
                    municipalityCode:"112233",
                    name:"Comunal"
                },
                {
                    municipalityCode:"112233",
                    name:"Guinobatan"
                },
                {
                    municipalityCode:"112233",
                    name:"Gulod"
                },
                {
                    municipalityCode:"112233",
                    name:"Gutad"
                },
                {
                    municipalityCode:"112233",
                    name:"Ibaba East"
                },
                {
                    municipalityCode:"112233",
                    name:"Ibaba West"
                },
                {
                    municipalityCode:"112233",
                    name:"Ilaya"
                },
                {
                    municipalityCode:"112233",
                    name:"Lalud"
                },
                {
                    municipalityCode:"112233",
                    name:"Lazareto"
                },
                {
                    municipalityCode:"112233",
                    name:"Libis"
                },
                {
                    municipalityCode:"112233",
                    name:"Lumangbayan"
                },
                {
                    municipalityCode:"112233",
                    name:"Mahal Na Pangalan"
                },
                {
                    municipalityCode:"112233",
                    name:"Maidlang"
                },
                {
                    municipalityCode:"112233",
                    name:"Malad"
                }
                )
             let html = `<option value="" disabled>Please select barangay</option>`
             let objResult = obj.filter(data => { return data.municipalityCode == cityCode})
             objResult.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#brgy').html(html)
        }
    });
}

$('#brgy').change(function(){
    $('#bry').val($( "#brgy option:selected" ).text())
})

function contactValidator(){
    let contact = $('#contact').val()
    let format = contact.slice(0, 2);
    console.log(format)
    if(format == '09' || format == '63'){
    }else{
       // alert('invalid format number. Number should start at 09 or 63')
        $('#err-contact').html('Number should start at 09 or 63')
        return false
    }

}

function looksLikeMail(str) {
    var lastAtPos = str.lastIndexOf('@');
    var lastDotPos = str.lastIndexOf('.');
    return (lastAtPos < lastDotPos && lastAtPos > 0 && str.indexOf('@@') == -1 && lastDotPos > 2 && (str.length - lastDotPos) > 2);
}