$(document).ready(function() {
   

    var table = $('#services_table').DataTable({
        processing: true,
        serverSide: true,
        responsive: false,
        autoWidth: false,
        buttons: false,
        order: [
            [0, 'asc']
        ],
        ajax: {
            url: "/get_doctors",
            error: function(xhr) {
                if (xhr.status == 401) {
                    window.location.replace("/login");
                } else {
                    toastr.error('An error occured, please try again later');
                }
            }
        },
        columns: [{
                data: 'fname',
                name: 'fname',
                searchable: true
            },
            {
                data: "mname",
                name: "mname",
                searchable: true
            },
            {
                data: "lname",
                name: "lname",
                searchable: true
            },
            {
                data: "st",
                name: "st",
                searchable: true
            },
            {
                data: "province",
                name: "province",
                searchable: true
            },
            {
                data: "municipality",
                name: "municipality",
                searchable: true
            },
            {
                data: "brgy",
                name: "brgy",
                searchable: true
            },
			{
                data: "contact",
                name: "contact",
                searchable: true
            },
            {
                data: "email",
                name: "email",
                searchable: true
            },
            {
                data: "birthdate",
                name: "birthdate",
                searchable: true
            },
			{
                data: "created_at",
                name: "created_at",
                searchable: true,
                class: 'v-middle',
                render: function(data, type) {
                    return type === 'sort' ? data : moment(data).isValid() ? moment(data).format('ll') : '---';
                }

            }, 
			{
            data: null,
            orderable: false,
            searchable: false,
            class: 'text-right',
            render: function(data, type, row) {
               
                    return '<button type="button"  data-id=' + data.id 
                                    + '" data-fname="' + data.fname 
                                    + '" data-mname="' + data.mname
                                    + '" data-lname="' + data.lname
                                    + '" data-st="' + data.st
                                    + '" data-province="' + data.province 
                                    + '" data-municipality="' + data.municipality
                                    + '" data-brgy="' + data.brgy 
                                    + '" data-contact="' + data.contact
                                    + '" data-email="' + data.email
                                    + '" data-birthdate="' + data.birthdate
                                    + '"  data-bs-toggle="modal" data-bs-target="#modal_add" class="edit btn btn-sm btn-secondary"><i class="fa fa-pencil-alt"></i></button> ';
				

				}
			}
        ],
        drawCallback: function(settings, json) {
            $('.tooltips').tooltip();
        },

    });

    $('#new').on('click', function(e) {
        $('#data_id').val(0);
        $('#fname').val('');
        $('#mname').val('');
        $('#lname').val('');
        $('#st').val('');
        $('#province').val('');
        $('#city').val('');
        $('#brgy').val('');
        $('#contact').val('');
        $('#birthdate').val('');
        $('#email').val('');
        $('.form-password-toggle').show();
    })

    $('#save_btn').on('click', function(e) {
        
        e.preventDefault();

        var btn = $(this);
    var form = $(this).closest('form');

        form.validate({
            rules: {
                service_name: {
                    required: true,
                
                },
                price: {
                    required: true,
                
                }
            }
        });

        if (!form.valid()) {
            return;
        }


        btn.attr('disabled', true);
        var data = new FormData(form[0]);

        //$('#msg').css('display', 'block');
        let url = $('#data_id').val() == 0 ? '/createDoctor' : '/updateDoctor'

        $.ajax({
            type: 'POST',
            url: url,
            data: data,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#msg').empty();
            },
            success: function(result) {
                //toastr.clear();
                if(result != 'success'){
                    alert(result)
                }else{
                    alert('save')
                    window.location.reload();
                }
                btn.attr('disabled', false);
            }
        });
      
    });

   

    // Edit - with universal route
    $('table tbody').on('click', '.edit', function() {
        console.log($(this).data('fname'))
        $('#data_id').val($(this).data('id'));
        $('.form-password-toggle').hide();
        $('#fname').val($(this).data('fname'));
        $('#mname').val($(this).data('mname'));
        $('#lname').val($(this).data('lname'));
        $('#st').val($(this).data('st'));
        $("#province option:contains(" + $(this).data('province') + ")").attr('selected', 'selected');
        $('#province').change();
        $("#city option:contains(" + $(this).data('municipality') + ")").attr('selected', 'selected');
        $('#city').change();
        $("#brgy option:contains(" + $(this).data('brgy') + ")").attr('selected', 'selected');
        $('#contact').val($(this).data('contact'));
        $('#birthdate').val($(this).data('birthdate'));
        $('#email').val($(this).data('email'));
       
    });

    $('#btn_update').click(function(e){
        e.preventDefault();

        var btn = $(this);
    var form = $(this).closest('form');

        form.validate({
            rules: {
                service_name: {
                    required: true,
                
                },
                price: {
                    required: true,
                
                }
            }
        });

        if (!form.valid()) {
            return;
        }


        btn.attr('disabled', true);
        var data = new FormData(form[0]);

        //$('#msg').css('display', 'block');

        $.ajax({
            type: 'POST',
            url: '/updateService',
            data: data,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#msg').empty();
            },
            success: function(result) {
                //toastr.clear();
                if(result != 'success'){
                    alert(result)
                }else{
                    alert('save')
                    window.location.reload();
                }
                btn.attr('disabled', false);
            }
        });
    })

});

var province = []
var municipalities = []
var brgy = []
getAllProvinces()
getAllMunicipality()
getAllBarangay()
$('#province').change(function(){
   
      let html = ''
             let objResult = municipalities.filter(data => { return data.provinceCode == this.value})
             objResult.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#city').html(html)
    $('#prov').val($( "#province option:selected" ).text())
})


function getAllProvinces(){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/provinces',
        success: function(result) {
             let obj = JSON.parse(result)
             let html = ''
                obj.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#province').html(html)
        }
    });
}

function getAllMunicipality(){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/municipalities',
        success: function(result) {
             let obj = JSON.parse(result)
             municipalities = obj
        }
    });
}

$('#city').change(function(){
      let html = ''
             let objResult = brgy.filter(data => { return data.municipalityCode == this.value})
             objResult.map(data => {
                    html += `<option value="${data.code}">${data.name}</option>`
                   
                })

            $('#brgy').html(html)
    $('#ct').val($( "#city option:selected" ).text())
})

function getAllBarangay(){
    $.ajax({
        type: 'GET',
        url: 'https://psgc.gitlab.io/api/barangays',
        success: function(result) {
             let obj = JSON.parse(result)
             brgy = obj
           
        }
    });
}

$('#brgy').change(function(){
    $('#bry').val($( "#brgy option:selected" ).text())
})
