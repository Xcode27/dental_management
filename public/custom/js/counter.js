var cur_num
var next_num
var stopBlinking = false;
var x = document.getElementById("myAudio"); 
$(document).ready(function(){
    getCurrentNumber();

    $('#next').click(function(){
        $.ajax({
            url: '/nextTransaction', 
            type: 'POST', 
            dataType: 'json',
            data: { 
                cur_num : cur_num,
            }, 
            success: function (data) { 
                console.log(data)
                if(data == 'success'){
                    getCurrentNumber()
                }else{
                    alert('Error');
                }
                
            },
            error: function(response) {
                getCurrentNumber()
                setTimeout(function() 
                {
                  stopBlinking = true;
                }, 7000);

                blink("#current_number");
                
            }
        });
    })
})

function getCurrentNumber(){
    $.ajax({
        type: 'GET',
        url: '/currentNumber',
        success: function(result) {
            let active_counter = 0;
            if(result == 0){
                active_counter = 1 
                $('#next_number').html(2)
            }else{
                active_counter = result
                result++
                $('#next_number').html(result)
            }
            cur_num = active_counter
            $('#current_number').html(active_counter)
        }
    });
}

function blink(selector) {
    $(selector).fadeOut('slow', function() {
        $(this).fadeIn('slow', function() {
            if (!stopBlinking)
            {
                blink(this);
                playAudio()
            }

            
        });
    });
}

function playAudio() { 
  x.play(); 
}