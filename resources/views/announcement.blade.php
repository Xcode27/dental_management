@extends('layouts.master')
@section('title', "Change Password")
@section('maintenance', "active open")
@section('settings', "active")
@section('specific-css')
<style>

</style>
@endsection
@section('main_content')
<div class="card mb-4">
 

  <div class="card-body">

    <h4 class="text-primary bold">Announcement</h4>

    <!-- <form  class="mb-3" data-parsley-validate> -->
        @csrf
        <div class="mb-3 col-md-12">
        <!-- <label class="form-label">  </label> -->
            <textarea class="form-control"  name="announcement" id="announcement" placeholder="Enter announcement" required rows="5" cols="50"></textarea>
        </div>
        <div class="mt-2">
            <button type="button" class="btn btn-primary me-2" id="save_announcement">Submit</button>
            <button type="reset" class="btn btn-outline-secondary">Cancel</button>
        </div>
    <!-- </form> -->
   
  </div>
  <!-- /Account -->
</div>

</div>
@endsection

@section('specific-js')
<script src="{{ asset('js/parsley.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/jquery.form.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('custom/js/change_pass.js')}}" type="text/javascript"></script>

@endsection