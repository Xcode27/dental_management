<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <title>DENTAL MANAGEMENT SYSTEM</title>

  <meta name="description" content="" />

  <!-- Favicon -->
  <link rel="icon" type="image/x-icon" href="{{asset('asset/img/favicon/favicon.ico')}}" />


  <!-- Icons. Uncomment required icon fonts -->
  <link rel="stylesheet" href="{{asset('asset/fonts/boxicons.css')}}" />

  <!-- Core CSS -->
  <link rel="stylesheet" href="{{asset('asset/css/core.css')}}" class="template-customizer-core-css" />
  <link rel="stylesheet" href="{{asset('asset/css/theme-default.css')}}" class="template-customizer-theme-css" />
  {{-- <link rel="stylesheet" href="{{asset('asset/css/demo.css')}}" /> --}}

  <!-- Vendors CSS -->
  <link rel="stylesheet" href="{{asset('asset/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />

  <!-- Page CSS -->
  {{-- <link href="{{ asset('css/bootstrap.css') }}" rel="stylesheet"> --}}
  <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/parsley.css')}}" rel="stylesheet" type="text/css" />

  <link href="{{ asset('custom/css/custom.css')}}" rel="stylesheet" type="text/css" />

  <!-- Page -->
  <link rel="stylesheet" href="{{asset('asset/css/pages/page-auth.css')}}" />
  <!-- Helpers -->
  <script src="{{asset('asset/js/helpers.js')}}"></script>

  <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
  <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
  <script src="{{asset('asset/js/config.js')}}"></script>
  <style>
    .error-msg {
      color:red;
    }
  </style>
</head>

<body>
  <!-- Content -->

  <div class="container-xxl">
    <center>
    <div class="container-p-y" style="width:600px">
      <div class="authentication-inner">
        <!-- Register -->
        <div class="card">
          <div class="card-body">
            <!-- Logo -->
            <div class="app-brand justify-content-center">
              <a href="index.html" class="app-brand-link gap-2">
              <img src="{{asset('home_image/logo.jpg')}}" width="350" height="150" alt="hero banner" class="w-100">
              </a>
            </div>

            
            <div id="msg"></div>
            <form class="mb-3" data-parsley-validate>
              @csrf
              <div class="row">
                  <div class="col-lg-6 col-md-6 col-12 order-0 mb-4">
                        <div class="mb-3">
                            <label for="email" class="form-label">First Name</label>
                            <input type="text" class="form-control" id="fname" name="fname" autofocus autocomplete="off"  />
                            <span class="error-msg" id="err-fname"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Middle Name (Optional)</label>
                            <input type="text" class="form-control" name="mname" autofocus autocomplete="off"  />
                            
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Last Name</label>
                            <input type="text" class="form-control" id="lname" name="lname" autofocus autocomplete="off"  />
                            <span class="error-msg" id="err-lname"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Contact</label>
                            <input type="text" class="form-control" id="contact" name="contact" placeholder="ex. 639261231256" id="contact" autofocus autocomplete="off"  onblur="contactValidator()"/>
                            <span class="error-msg" id="err-contact"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Birthdate</label>
                            <input type="date" class="form-control" id="birthdate" name="birthdate" autofocus autocomplete="off"  />
                            <span class="error-msg" id="err-birthdate"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Street/Blk/Lot</label>
                            <input type="text" class="form-control" name="st" autofocus autocomplete="off"  />
                          </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-12 order-0 mb-4">
                          
                          <div class="mb-3">
                            <label for="email" class="form-label">Province</label>
                            <select class="form-select" id="province" name="province" >
                            </select>
                            <input type="hidden" class="form-control" id="prov" name="prov"/>
                            <span class="error-msg" id="err-province"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Municipality</label>
                            <select class="form-select" id="city" name="city" >
                            </select>
                            <input type="hidden" class="form-control" id="ct" name="ct"/>
                            <span class="error-msg" id="err-municipality"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Brgy.</label>
                            <select class="form-select" id="brgy" name="brgy" >
                            </select>
                            <input type="hidden" class="form-control" id="bry" name="bry"/>
                            <span class="error-msg" id="err-brgy"></span>
                          </div>
                          <div class="mb-3">
                            <label for="email" class="form-label">Email address</label>
                            <input type="text" class="form-control" id="email" name="email" autofocus autocomplete="off"   />
                            <span class="error-msg" id="err-email"></span>
                          </div>
                          <div class="mb-3 form-password-toggle" style="padding-top:5px;">
                            <div class="d-flex justify-content-between">
                              <label class="form-label">Password</label>
                            </div>
                            <div class="input-group input-group-merge">
                              <input type="password" class="form-control" id="pass" name="password" autocomplete="off"  />
                              <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                            </div>
                            <span class="error-msg" id="err-password"></span>
                          </div>
                          <div class="mb-3 form-password-toggle" style="padding-top:5px;">
                            <div class="d-flex justify-content-between">
                              <label class="form-label">Confirm Password</label>
                            </div>
                            <div class="input-group input-group-merge">
                              <input type="password" class="form-control" id="confirm" name="confirm" autocomplete="off"  />
                              <span class="input-group-text cursor-pointer"><i class="bx bx-hide"></i></span>
                            </div>
                            <span class="error-msg" id="err-confirm"></span>
                          </div>
                  </div>
              </div>
             
              <div class="mb-3">
                <button class="btn btn-primary d-grid w-100" id="register_btn" type="submit">Register</button>
              </div>
              
            </form>
          </div>
        </div>
        <!-- /Register -->
      </div>
    </div>
</center>
  </div>

  <!-- / Content -->

  <!-- Core JS -->
  <!-- build:js assets/vendor/js/core.js -->
  <script src="{{asset('asset/libs/jquery/jquery.js')}}"></script>
  <script src="{{asset('asset/libs/popper/popper.js')}}"></script>
  <script src="{{asset('asset/js/bootstrap.js')}}"></script>
  <script src="{{asset('asset/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

  <script src="{{asset('asset/js/menu.js')}}"></script>


  <!-- endbuild -->

  <!-- Vendors JS -->

  <!-- Main JS -->
  <script src="{{asset('asset/js/main.js')}}"></script>

  {{-- <script src="{{asset('asset/js/ui-toasts.js')}}"></script> --}}


  {{-- <script src="{{ asset('js/bootstrap.js') }}" type="text/javascript"></script> --}}
  <script src="{{ asset('js/toastr.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/parsley.min.js')}}" type="text/javascript"></script>
  <script src="{{ asset('js/jquery.form.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('js/jquery.validate.min.js') }}" type="text/javascript"></script>

  <!-- Nex Library -->
  <script src="{{ asset('js/nexlibrary.js') }}" type="text/javascript"></script>
  <script src="{{ asset('custom/js/login.js') }}" type="text/javascript"></script>

</body>

</html>