@extends('layouts.master')
@section('title', "Dashboard")
@section('dashboard', "active")
@section('specific-css')
{{-- <link rel="stylesheet" href="{{ asset('asset/vendor/libs/apex-charts/apex-charts.css')}}" /> --}}
<style>
  .tab-pane ul,
  .handlers-container {
    max-height: 25vw;
  }
  .dashboard{
    cursor: pointer;
  }
</style>
@endsection
@section('main_content')

<!-- contents -->
<div class="row">
  <div class="col-lg-4 col-md-4 col-12 order-0 mb-4">
    <div class="card">
      <div class="card-header bg-primary">
        <h5 class=" semi-bold text-white m-0">Next Number Serving</h5>
      </div>
    </div>
    <div class="nav-align-top mb-4">
      <div class="card p-3 overflow-auto">
        <center><h3 class=" semi-bold text-dark m-0" id="next_number">0</h3></center>
        
      </div>
    </div>

  </div>
  

  <div class="col-lg-8 col-md-8 col-12 order-0 mb-4">
    <div class="card">
      <div class="card-header bg-primary">
        <h5 class=" semi-bold text-white m-0">Current Number Serving</h5>
      </div>
    </div>
    <audio id="myAudio">
            <source src="./doorbell.mp3" type="audio/mpeg">
    </audio>
    <div class="nav-align-top mb-4">
      <div class="card p-3 overflow-auto">
        <center>
            <h1 class=" semi-bold text-dark m-0" id="current_number">0</h1><br>
            <button class="btn btn-primary d-grid w-100" id="next" type="submit">Next</button>
        </center>
      </div>
    </div>

  </div>
</div>


@endsection

@section('specific-js')


<script src="{{ asset('custom/js/counter.js') }}" type="text/javascript"></script>
@endsection