<!DOCTYPE html>

<html>

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

  <title>COUNTER WINDOW SYSTEM</title>

  <meta name="description" content="" />
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Favicon -->
  <link rel="icon" type="image/x-icon" href=" {{asset('images/favicon.jpg')}}" />

  <!-- Icons. Uncomment required icon fonts -->
  <link rel="stylesheet" href="{{asset('/asset/fonts/boxicons.css')}}" />

  <!-- Core CSS -->
  <link rel="stylesheet" href="{{asset('/asset/css/core.css')}}" class="template-customizer-core-css" />
  <link rel="stylesheet" href="{{asset('/asset/css/theme-default.css')}}" class="template-customizer-theme-css" />

  <!-- Vendors CSS -->
  <link rel="stylesheet" href="{{asset('/asset/libs/perfect-scrollbar/perfect-scrollbar.css')}}" />

  <link rel="stylesheet" href="{{asset('/asset/libs/apex-charts/apex-charts.css')}}" />

  <!-- Page CSS -->
  <link href="{{ asset('css/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/select2.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/toastr.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('css/parsley.css')}}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('asset/libs/datatable/datatables.min.css')}}" rel="stylesheet" type="text/css" />

  <link href="{{ asset('custom/css/custom.css')}}" rel="stylesheet" type="text/css" />


  @yield('specific-css')

  <!-- Helpers -->
  <script src="{{asset('/asset/js/helpers.js')}}"></script>
  <script src="{{asset('/asset/js/config.js')}}"></script>
</head>

<body>
  <!-- Layout wrapper -->
  <div class="layout-wrapper layout-content-navbar">
    <div class="layout-container">
     

      <!-- Layout container -->
      <div class="layout-page">



        <!-- Content wrapper -->
        <div class="content-wrapper">
          <!-- Content -->

          <div class="container-xxl flex-grow-1 container-p-y">
          <div class="row">
              <div class="col-lg-4 col-md-4 col-12 order-0 mb-4">
                <div class="card">
                  <div class="card-header bg-primary">
                    <h5 class=" semi-bold text-white m-0">Next Number Serving</h5>
                  </div>
                </div>
                <div class="nav-align-top mb-4">
                  <div class="card p-3 overflow-auto">
                    <center><h3 class=" semi-bold text-dark m-0" id="next_number">0</h3></center>
                    
                  </div>
                </div>

              </div>
              

              <div class="col-lg-8 col-md-8 col-12 order-0 mb-4">
                <div class="card">
                  <div class="card-header bg-primary">
                    <h5 class=" semi-bold text-white m-0">Current Number Serving</h5>
                  </div>
                </div>
                <div class="nav-align-top mb-4">
                  <div class="card p-3 overflow-auto">
                    <center>
                        <h1 class=" semi-bold text-dark m-0" id="current_number">0</h1><br>
                    </center>
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
          
        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>
      </div>
    </div>

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('asset/vendor/libs/jquery/jquery.js')}}"></script>
    <script src="{{ asset('asset/vendor/libs/popper/popper.js')}}"></script>
    <script src="{{ asset('asset/vendor/js/bootstrap.js')}}"></script>
    <script src="{{ asset('asset/vendor/libs/perfect-scrollbar/perfect-scrollbar.js')}}"></script>

    <script src="{{ asset('asset/vendor/js/menu.js')}}"></script>
    <!-- endbuild -->

    <!-- Vendors JS -->
    <script src="{{ asset('asset/vendor/libs/apex-charts/apexcharts.js')}}"></script>

    <!-- Main JS -->
    <script src="{{ asset('asset/js/main.js')}}"></script>

    <!-- Page JS -->
    <script src="{{ asset('asset/js/dashboards-analytics.js')}}"></script>

    <script src="{{ asset('asset/libs/datatable/datatables.min.js') }}" type="text/javascript"></script>

    <script src="{{ asset('js/moment.min.js') }}"></script>
    <script src="{{ asset('js/select2.full.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/toastr.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/parsley.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/jquery-idleTimeout.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/bootstrap-datepicker.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/store.min.js')}}" type="text/javascript"></script>
    <script src="{{ asset('js/sticky.min.js')}}" type="text/javascript"></script>
    <!-- Nex Library -->
    <script src="{{ asset('js/fileupload.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/nexlibrary.js') }}" type="text/javascript"></script>
    <script src="https://js.pusher.com/7.2/pusher.min.js"></script>

    @yield('specific-js')
    <script type="text/javascript">
      var stopBlinking = false;
      
      $(document).ready(function() {
      
        getCurrentNumber(0);
        var pusher = new Pusher('c3bd4415a313dc16311b', {
        cluster: 'ap1',
        encrypted: true
      });
      
        var announce = pusher.subscribe('dental_management');

        announce.bind('announce-number', function(data) {
          console.log(data)
          getCurrentNumber(1);
          
        })

      });

      function getCurrentNumber(control){
        $.ajax({
            type: 'GET',
            url: '/currentNumber',
            success: function(result) {
                let active_counter = 0;
                if(result == 0){
                    active_counter = 1 
                    $('#next_number').html(2)
                }else{
                    active_counter = result
                    result++
                    $('#next_number').html(result)
                }
                $('#current_number').html(active_counter)
                if(control > 0){

                  setTimeout(function() 
                    {
                      stopBlinking = true;
                    }, 7000);

                    blink("#current_number");
                    
                  }
                  
               
            }
        });
    }

        function blink(selector) {
          $(selector).fadeOut('slow', function() {
              $(this).fadeIn('slow', function() {
                  if (!stopBlinking)
                  {
                      blink(this);
                      
                  }

                  
              });
          });
      }

      function playAudio() { 
        x.play(); 
      } 
    </script>

</body>

</html>