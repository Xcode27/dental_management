

  <main>
    <article>

      <!-- 
        - #HERO
      -->

      <section class="section hero" id="home" style="background-image: 'url('./home_image/hero-bg.png')"
        aria-label="hero">
        <div class="container">

          <div class="hero-content">

            <p class="section-subtitle">Ponce-Miranda Dental Clinic</p>

            <h1 class="h1 hero-title">We Are Best Dental Service</h1>

            <form action="/login" method="">
            <button type="submit"  class="btn">Book appointment</button>
        </form>
          </div>
          
          <figure class="hero-banner">
            <br>
            <img src="{{asset('home_image/logo.jpg')}}" width="587" height="839" alt="hero banner" class="w-100">
          </figure>

        </div>
      </section>