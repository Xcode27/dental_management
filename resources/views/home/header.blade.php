
<!--header--->
    <div class="header-bottom" data-header>
      <div class="container">

        <!-- <a href="/" class="navbar-link"><img src="{{asset('home_image/logo.jpg')}}" height="80" width="120"></a> -->
  
        <nav class="navbar container" data-navbar>
          <ul class="navbar-list">
            
              <a href="/" class="navbar-link" data-nav-link>Home</a>
            </li>

            <li>
              <a href="/" class="navbar-link" data-nav-link>Services</a>
            </li>

            <li>
              <a href="/" class="navbar-link" data-nav-link>About Us</a>
            </li>


            <li>
              <a href="/" class="navbar-link" data-nav-link>Contact</a>
            </li>

          </ul>
        </nav>

        <a href="/login" class="login">Log in</a>
        <a href="/register" class="register">Register</a>

        <button class="nav-toggle-btn" aria-label="Toggle menu" data-nav-toggler>
          <ion-icon name="menu-sharp" aria-hidden="true" class="menu-icon"></ion-icon>
          <ion-icon name="close-sharp" aria-hidden="true" class="close-icon"></ion-icon>
        </button>

      </div>
    </div>

  </header>


